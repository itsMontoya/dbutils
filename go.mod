module gitlab.com/itsMontoya/dbutils

go 1.13

require (
	github.com/Hatch1fy/errors v0.1.0
	github.com/boltdb/bolt v1.3.1
)
